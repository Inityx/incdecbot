use std::time::{Duration, Instant};

pub struct Metrics {
    start: Instant,
    framework: Duration,
}

impl Metrics {
    pub fn new() -> Self {
        Self {
            start: Instant::now(),
            framework: Duration::ZERO,
        }
    }

    pub fn time_framework(&mut self) -> Stopwatch<'_> {
        Stopwatch::incrementing(&mut self.framework)
    }
}

impl Drop for Metrics {
    fn drop(&mut self) {
        let total = self.start.elapsed();
        let bot = total - self.framework;
        println!("METRIC: bot_ms {} framework_ms {}", bot.as_millis(), self.framework.as_millis());
    }
}

pub struct Stopwatch<'acc> {
    acc: &'acc mut Duration,
    start: Instant,
}

impl<'acc> Stopwatch<'acc> {
    fn incrementing(acc: &'acc mut Duration) -> Self {
        Self { acc, start: Instant::now() }
    }
}

impl Drop for Stopwatch<'_> {
    fn drop(&mut self) {
        *self.acc += self.start.elapsed();
    }
}
