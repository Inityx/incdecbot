use frankenstein::User;

pub trait UserExt {
    fn display_name(&self) -> String;
}

impl UserExt for User {
    fn display_name(&self) -> String {
        if let Some(username) = &self.username {
            return format!("@{username}");
        }

        if let Some(last_name) = &self.last_name {
            format!("{} {}", self.first_name, last_name)
        } else {
            self.first_name.clone()
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    const USERNAME: &str = "foo";
    const FIRST_NAME: &str = "bar";
    const LAST_NAME: &str = "baz";

    fn user_with(username: bool, last_name: bool) -> User {
        User {
            username: username.then(|| USERNAME.to_string()),
            first_name: FIRST_NAME.to_string(),
            last_name: last_name.then(|| LAST_NAME.to_string()),
            id: 0,
            is_bot: false,
            language_code: None,
            is_premium: None,
            added_to_attachment_menu: None,
            can_join_groups: None,
            can_read_all_group_messages: None,
            supports_inline_queries: None,
        }
    }

    #[test]
    fn with_username() {
        assert_eq!(
            format!("@{USERNAME}"),
            user_with(true, true).display_name(),
        );
    }

    #[test]
    fn with_first_last() {
        assert_eq!(
            format!("{FIRST_NAME} {LAST_NAME}"),
            user_with(false, true).display_name(),
        );
    }

    #[test]
    fn with_first() {
        assert_eq!(
            FIRST_NAME,
            user_with(false, false).display_name(),
        );
    }
}
