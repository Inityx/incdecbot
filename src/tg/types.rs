use std::ops::Range;

use frankenstein::{MessageEntity, Message, MessageEntityType};

pub struct IndexedText<'message> {
    full_text: &'message str,
    entities: &'message [MessageEntity],
    command: Option<&'message str>,
    body: &'message str,
}

impl<'message> IndexedText<'message> {
    const COMMAND_PREFIX: &'static str = "/";

    pub fn command_body(&self) -> (Option<&'message str>, &'message str) {
        (self.command, self.body)
    }

    pub fn from_message(message: &'message Message) -> Option<Self> {
        message.text.as_deref().map(|text| {
            let entities = message.entities.as_deref().unwrap_or_default();
            let first_command = entities.iter().find(|entity|
                entity.type_field == MessageEntityType::BotCommand
            );

            let (command, body) = match first_command {
                None => (None, text.trim()),
                Some(entity) => {
                    let start =         entity.offset as usize + Self::COMMAND_PREFIX.len();
                    let end   = start + entity.length as usize - Self::COMMAND_PREFIX.len();
                    (Some(&text[start..end]), text[end..].trim())
                }
            };

            Self { full_text: text, entities, command, body }
        })
    }

    pub fn mention_positions(&self) -> impl 'message + Iterator<Item=Range<usize>> {
        self.entities.iter()
            .filter(|entity| entity.type_field == MessageEntityType::Mention)
            .map(|&MessageEntity { offset, length, .. }|
                offset as usize..(offset + length) as usize
            )
    }

    pub fn mentions(&self) -> impl 'message + Iterator<Item=&'message str> {
        let text = self.full_text; // needed so enforce cloning the &str out of self
        self.mention_positions().map(move |range| &text[range])
    }
}
