#![feature(round_char_boundary, let_chains)]
#![deny(clippy::all)]

use std::{env, io::Write, path::Path};

mod bot;
mod tg;
mod util;

use anyhow::Context;
use bot::{
    db::photo::PhotoDB,
    Bot,
};

const TOKEN_ENV: &str = "TELEGRAM_BOT_TOKEN";
const PHOTOS_DIR: &str = "photos";

#[tokio::main(flavor="current_thread")]
async fn main() -> anyhow::Result<()> {
    let api_token = env::var(TOKEN_ENV).context("Unable to read API token from env TELEGRAM_BOT_TOKEN")?;
    let api = frankenstein::AsyncApi::new(&api_token);

    let photos_dir = Path::new(PHOTOS_DIR);
    let nut_photos = PhotoDB::new(photos_dir.join("nut")).context("Nut photos error")?;

    println!("INFO: Environment loaded, starting bot");
    Bot::new(api, nut_photos).await.run().await;
    println!("INFO: Bot shutdown gracefully");

    std::io::stdout().flush()?;
    Ok(())
}
