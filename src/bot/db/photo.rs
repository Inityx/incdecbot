use std::{
    collections::hash_map::{Entry, HashMap}, ffi::OsStr, fs::{self, File}, io::{self, BufRead, BufReader}, path::{Path, PathBuf}
};

use itertools::Itertools;
use sha2::{Sha256, Digest};
use rand::thread_rng;

type Sha2Hash = [u8;32];

const INDEX_FILE_NAME: &str = "index.json";
const IMAGE_FILE_EXTENSIONS: [&str; 3] = ["png", "jpg", "gif"];

#[derive(Clone, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct ImageMetadata {
    pub path: PathBuf,
    pub hash: Sha2Hash,
    pub tg_file_id: Option<String>,
    pub dc_file_id: Option<String>,
}

impl ImageMetadata {
    pub fn uncached(path: PathBuf, hash: Sha2Hash) -> Self {
        Self { path, hash, tg_file_id: None, dc_file_id: None }
    }
}

impl From<ImageMetadata> for frankenstein::api_params::File {
    fn from(value: ImageMetadata) -> Self {
        match value.tg_file_id {
            Some(file_id) => Self::from(file_id),
            None => Self::from(value.path),
        }
    }
}

pub struct PhotoDB {
    dir: PathBuf,
    images: HashMap<PathBuf, ImageMetadata>,
    dirty: bool,
}

impl PhotoDB {
    pub fn new(dir: impl AsRef<Path>) -> io::Result<Self> {
        let dir = dir.as_ref().to_path_buf();
        let index_path = dir.join(INDEX_FILE_NAME);

        if index_path.is_file() {
            println!("INFO: Photo index found, reading from {}", index_path.display());
            let index: Vec<ImageMetadata> = serde_json::from_reader(File::open(index_path)?)?;

            let images: HashMap<_, _> = index.into_iter().map(|md| (md.path.clone(), md)).collect();

            println!("INFO: Read {} photos from file", images.len());
            Ok(Self { dir, images, dirty: false })
        } else {
            println!("WARN: Creating photo index at {}", index_path.display());
            Ok(Self { dir, images: HashMap::new(), dirty: true })
        }
    }

    pub fn sync_fs(&mut self, force: bool) {
        if let Err(e) = self.scan_fs() {
            println!("ERROR: Failed nut photos scan: {}", e);
        }

        if let Err(e) = self.persist_cached(force) {
            println!("ERROR: Failed nut photos index sync: {}", e);
        }
    }

    fn scan_fs(&mut self) -> io::Result<()> {
        { // Remove files that no longer exist
            self.images.retain(|_, md| md.path.is_file());
        }

        let image_paths = fs::read_dir(&self.dir)?
            .map_ok(|entry| entry.path())
            .filter_ok(|path| is_image(path))
            .collect::<Result<Vec<_>, _>>()?;

        let mut new = 0usize;
        let mut uncached = 0usize;
        let mut modified = 0usize;

        // Observe new and changed files from disk
        for path in image_paths {
            let file_hash = file_hash(&path)?;

            match self.images.entry(path.clone()) {
                Entry::Vacant(entry) => {
                    println!("INFO:   New photo {}", path.display());
                    entry.insert(ImageMetadata::uncached(path, file_hash));
                    new += 1;
                }
                Entry::Occupied(mut entry) => {
                    let value = entry.get();
                    if value.hash != file_hash {
                        println!("INFO:   Modified photo {}", path.display());
                        modified += 1;
                        entry.insert(ImageMetadata::uncached(path, file_hash));
                    } else if value.tg_file_id.is_none() && value.dc_file_id.is_none() {
                        println!("INFO:   Uncached photo {}", path.display());
                        uncached += 1;
                    }
                }
            };
        }

        println!("INFO: Scanned images in {}: {new} new, {uncached} uncached, and {modified} modified", self.dir.display());
        if modified > 0 {
            self.dirty = true;
        }

        Ok(())
    }

    fn persist_cached(&mut self, force: bool) -> io::Result<()> {
        if !(self.dirty || force) { return Ok(()) }

        let index_path = self.dir.join(INDEX_FILE_NAME);
        println!("INFO: Synchronizing to {}", index_path.display());

        let entries = self.images.values().collect::<Vec<_>>();

        serde_json::to_writer(File::create(index_path)?, &entries)?;

        self.dirty = false;
        println!("INFO: Synchronized nut database");
        Ok(())
    }

    pub fn get_random(&mut self) -> Option<ImageMetadata> {
        use rand::seq::IteratorRandom;
        self.images.values().choose(&mut thread_rng()).cloned()
    }

    pub fn mark_cached_tg(&mut self, path: &Path, new_file_id: String) {
        let Some(ImageMetadata { tg_file_id, .. }) = self.images.get_mut(path) else {
            println!("WARN: Attempted to cache non-indexed photo {}", path.display());
            return;
        };

        *tg_file_id = Some(new_file_id);

        self.dirty = true;
    }
}

fn file_hash(path: &Path) -> io::Result<Sha2Hash> {
    let mut reader = BufReader::new(File::open(path)?);
    let mut hasher = Sha256::new();

    while let data = reader.fill_buf()? && !data.is_empty() {
        hasher.update(data);
        let len = data.len();
        reader.consume(len);
    }

    Ok(hasher.finalize().into())
}

fn is_image(path: &Path) -> bool {
    path.extension()
        .and_then(OsStr::to_str)
        .map(|ext| IMAGE_FILE_EXTENSIONS.contains(&ext))
        .unwrap_or(false)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn index_entry_serde() {
        let index = vec![
            ImageMetadata {
                path: PathBuf::from("/path/to/Foo.jpg"),
                hash: Sha2Hash::default(),
                tg_file_id: Some("Bar".into()),
                dc_file_id: None,
            },
        ];

        let ser = serde_json::to_string(&index).unwrap();
        let de = serde_json::from_str::<Vec<ImageMetadata>>(&ser).unwrap();

        assert_eq!(index, de);
    }

    const ARRAY: Sha2Hash = [0;32];
    const SERIALIZED: &str = "[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]";

    #[test]
    fn sha2_hash_serialize() {
        assert_eq!(
            SERIALIZED.to_string(),
            serde_json::to_string(&ARRAY).unwrap(),
        );
    }

    #[test]
    fn sha2_hash_deserialize() {
        assert_eq!(
            ARRAY,
            serde_json::from_str::<Sha2Hash>(SERIALIZED).unwrap(),
        );
    }
}
