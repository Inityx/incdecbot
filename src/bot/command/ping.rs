use super::Command;
use crate::{bot::{algorithm::dingle, BotState, MessageAction}, util::Metrics};

use itertools::Itertools;
use std::{fmt::{self, Write}, iter};

pub struct SayHenlo;
pub struct SendNut;
pub struct ListHelp;
pub struct DingleGen;

impl fmt::Display for SayHenlo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("greeting")
    }
}

impl fmt::Display for SendNut {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("nut request")
    }
}

impl fmt::Display for ListHelp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("help message request")
    }
}

impl fmt::Display for DingleGen {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("Dingle request")
    }
}

impl Command for SayHenlo {
    async fn execute<M: MessageAction>(&self, message: M, _bot: &BotState, metrics: &mut Metrics) -> Result<(), M::Error> {
        message.reply_html("henlo friend".to_string(), metrics).await
    }
}

impl Command for SendNut {
    async fn execute<M: MessageAction>(&self, message: M, bot: &BotState, metrics: &mut Metrics) -> Result<(), M::Error> {
        let Some(photo) = bot.nut_photos.lock().unwrap().get_random() else {
            return message.reply_html("NUTN'T".to_string(), metrics).await;
        };

        message.send_photo(photo, bot, metrics).await
    }
}

impl Command for ListHelp {
    async fn execute<M: MessageAction>(&self, message: M, _bot: &BotState, metrics: &mut Metrics) -> Result<(), M::Error> {
        let mut text = String::from("Commands:\n");
        for command in super::name::ALL {
            writeln!(text, " • {command}").unwrap();
        }

        message.send_html(text, metrics).await
    }
}

impl Command for DingleGen {
    async fn execute<M: MessageAction>(&self, message: M, _bot: &BotState, metrics: &mut Metrics) -> Result<(), M::Error> {
        let names = iter::repeat_with(dingle::generate).take(10).join("\n");
        message.send_html(names, metrics).await
    }
}
