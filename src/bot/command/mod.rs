use enum_dispatch::enum_dispatch;
use frankenstein::Message;

use std::fmt;
use crate::{
    bot::{BotState, MessageAction},
    tg::types::IndexedText, util::Metrics,
};

mod transform;
mod ping;

mod name {
    pub const ROTATE: &str = "rotate";
    pub const IZZLE: &str = "izzle";
    pub const ZALGO: &str = "zalgo";
    pub const DINGLE: &str = "dingle";
    pub const SUBREDDIT: &str = "r";
    pub const UNSHORT: &str = "unshort";
    pub const NUT: &str = "nut";
    pub const MOCK: &str = "mock";
    pub const HELP: &str = "help";

    pub const ALL: &[&str] = &[ROTATE, IZZLE, ZALGO, DINGLE, SUBREDDIT, UNSHORT, NUT, MOCK];
}

#[enum_dispatch]
pub trait Command: Send + Sync {
    async fn execute<M: MessageAction>(&self, message: M, bot: &BotState, metrics: &mut Metrics) -> Result<(), M::Error>;
}

#[enum_dispatch(Command)]
pub enum CommandDispatch<'msg> {
    Rotate(transform::RotateText<'msg>),
    Izzle(transform::IzzleText<'msg>),
    Zalgo(transform::ZalgoText<'msg>),
    Mock(transform::MockText<'msg>),
    Subreddit(transform::LinkSubreddit<'msg>),
    Unshort(transform::Unshort<'msg>),
    Dingle(ping::DingleGen),
    Nut(ping::SendNut),
    Henlo(ping::SayHenlo),
    Help(ping::ListHelp),
}

impl<'msg> CommandDispatch<'msg> {
    /// Return Command or name of unknown command
    pub fn parse(message: &'msg Message, bot_name: &str)
    -> Option<Result<CommandDispatch<'msg>, &'msg str>>
    {
        let text = IndexedText::from_message(message)?;

        if let (Some(command_name), body) = text.command_body() {
            use name::*;
            Some(Ok(match command_name {
                ROTATE    => transform::RotateText(body).into(),
                IZZLE     => transform::IzzleText(body).into(),
                ZALGO     => transform::ZalgoText(body).into(),
                MOCK      => transform::MockText(body).into(),
                SUBREDDIT => transform::LinkSubreddit(body.split_whitespace().next()).into(),
                UNSHORT   => transform::Unshort::parse(body).into(),
                DINGLE    => ping::DingleGen.into(),
                NUT       => ping::SendNut.into(),
                HELP      => ping::ListHelp.into(),
                _         => return Some(Err(command_name)),
            }))
        }
        else if text.mentions().any(|username| username == bot_name) {
            Some(Ok(ping::SayHenlo.into()))
        }
        else {
            None
        }
    }
}

impl fmt::Display for CommandDispatch<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let value: &dyn fmt::Display = match self {
            Self::Rotate(ref v) => v,
            Self::Izzle(ref v) => v,
            Self::Zalgo(ref v) => v,
            Self::Mock(ref v) => v,
            Self::Subreddit(ref v) => v,
            Self::Unshort(ref v) => v,
            Self::Dingle(ref v) => v,
            Self::Nut(ref v) => v,
            Self::Henlo(ref v) => v,
            Self::Help(ref v) => v,
        };

        value.fmt(f)
    }
}
