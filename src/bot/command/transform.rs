use std::fmt;
use v_htmlescape::escape as html_escape;

use crate::{
    bot::{
        algorithm::{izzle::izzle, mock::mock, rotate, zalgo::zalgo, Truncate},
        command::Command,
        BotState, MessageAction,
    }, util::Metrics,
};

const SUBREDDIT_LENGTH: usize = 20;

#[derive(Debug, Clone, Copy, PartialEq, Eq)] pub struct RotateText<'msg>(pub &'msg str);
#[derive(Debug, Clone, Copy, PartialEq, Eq)] pub struct IzzleText<'msg>(pub &'msg str);
#[derive(Debug, Clone, Copy, PartialEq, Eq)] pub struct ZalgoText<'msg>(pub &'msg str);
#[derive(Debug, Clone, Copy, PartialEq, Eq)] pub struct MockText<'msg>(pub &'msg str);
#[derive(Debug, Clone, Copy, PartialEq, Eq)] pub struct LinkSubreddit<'msg>(pub Option<&'msg str>);
#[derive(Debug, Clone, Copy, PartialEq, Eq)] pub struct Unshort<'msg>(pub Option<&'msg str>);

impl fmt::Display for RotateText<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "request to rotate {:?}", Truncate::to_log(self.0))
    }
}

impl fmt::Display for IzzleText<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "request to izzle {:?}", Truncate::to_log(self.0))
    }
}

impl fmt::Display for ZalgoText<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "request to zalgo {:?}", Truncate::to_log(self.0))
    }
}

impl fmt::Display for MockText<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "request to mock {:?}", Truncate::to_log(self.0))
    }
}

impl fmt::Display for LinkSubreddit<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.0 {
            Some(sub) => write!(f, "request to link /r/{}", Truncate::at(sub, SUBREDDIT_LENGTH)),
            None => f.write_str("empty subreddit link"),
        }
    }
}

impl fmt::Display for Unshort<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.0 {
            Some(sub) => write!(f, "request to unshort {}", Truncate::at(sub, SUBREDDIT_LENGTH)),
            None => f.write_str("empty YouTube short link"),
        }
    }
}

impl Command for RotateText<'_> {
    async fn execute<M: MessageAction>(&self, message: M, _bot: &BotState, metrics: &mut Metrics) -> Result<(), M::Error> {
        let rotated = match rotate::Rotated::sentence(self.0) {
            Ok(rotated) => rotated,
            Err(error) => {
                println!("WARN: Rotation failed: {error}");
                return message.reply_html(format!("{error}\n\n<pre>DUMMY</pre>"), metrics).await;
            }
        };

        message.reply_html(
            format!("<pre>{}</pre>", html_escape(&rotated.to_string())),
            metrics,
        ).await
    }
}

impl Command for IzzleText<'_> {
    async fn execute<M: MessageAction>(&self, message: M, _bot: &BotState, metrics: &mut Metrics) -> Result<(), M::Error> {
        message.reply_html(izzle(self.0), metrics).await
    }
}

impl Command for ZalgoText<'_> {
    async fn execute<M: MessageAction>(&self, message: M, _bot: &BotState, metrics: &mut Metrics) -> Result<(), M::Error> {
        message.reply_html(zalgo(self.0), metrics).await
    }
}

impl Command for MockText<'_> {
    async fn execute<M: MessageAction>(&self, message: M, _bot: &BotState, metrics: &mut Metrics) -> Result<(), M::Error> {
        let text = match self.0 {
            "" => "/mock",
            s => s,
        };

        message.reply_html(v_htmlescape::escape(&mock(text)).to_string(), metrics).await
    }
}

impl Command for LinkSubreddit<'_> {
    async fn execute<M: MessageAction>(&self, message: M, _bot: &BotState, metrics: &mut Metrics) -> Result<(), M::Error> {
        let Some(sub) = self.0 else {
            return message.reply_html("SUBREDDIN'T".to_string(), metrics).await;
        };

        message.reply_html(
            format!(
                "<a href=\"https://old.reddit.com/r/{}\">/r/{}</a>",
                percent_encoding::utf8_percent_encode(sub, percent_encoding::NON_ALPHANUMERIC),
                v_htmlescape::escape(sub),
            ),
            metrics,
        ).await
    }
}

impl<'msg> Unshort<'msg> {
    pub fn parse(text: &'msg str) -> Self {
        Self(text
            .split_whitespace().next()
            .and_then(|chunk|
                chunk.strip_prefix("https://youtube.com/shorts/")
                    .or_else(|| chunk.strip_prefix("https://youtu.be/"))
            )
            .and_then(|suffix| suffix.split('?').next())
        )
    }
}

impl Command for Unshort<'_> {
    async fn execute<M: MessageAction>(&self, message: M, _bot: &BotState, metrics: &mut Metrics) -> Result<(), M::Error> {
        let Some(id) = self.0 else {
            return message.reply_html("SHORTN'T".to_string(), metrics).await;
        };

        message.reply_html(
            format!("https://youtube.com/watch?v={id}"),
            metrics,
        ).await
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn unshort() {
        assert_eq!(Unshort::parse("https://youtube.com/shorts/id-here"), Unshort(Some("id-here")));
        assert_eq!(Unshort::parse("https://youtube.com/shorts/id-here?feature=share"), Unshort(Some("id-here")));
        assert_eq!(Unshort::parse("https://youtu.be/id-here"), Unshort(Some("id-here")));
        assert_eq!(Unshort::parse("https://youtu.be/id-here?feature=share"), Unshort(Some("id-here")));
    }
}
