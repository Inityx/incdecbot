use rand::{thread_rng, Rng};

pub fn mock(text: &str) -> String {
    let mut mocked = String::with_capacity(text.len());
    let sputter = Sputter::starting_with(false, thread_rng());

    for (capital, ch) in sputter.zip(text.chars()) {
        if ch.is_whitespace() { mocked.push(ch); }
        // Separated because ToUppercase and ToLowercase are distinct types
        else if capital { mocked.extend(ch.to_uppercase()); }
        else            { mocked.extend(ch.to_lowercase()); }
    }

    mocked
}

struct Sputter<Rand: Rng> {
    rand: Rand,
    current: bool,
    streak: u8,
}

impl<Rand: Rng> Sputter<Rand> {
    const STREAK_LIMIT: u8 = 2;

    fn starting_with(initial: bool, mut rand: Rand) -> Self {
        Self {
            current: initial,
            streak: rand.gen::<u8>() % Self::STREAK_LIMIT,
            rand,
        }
    }
}

impl<Rand: Rng> Iterator for Sputter<Rand> {
    type Item = bool;

    fn next(&mut self) -> Option<Self::Item> {
        let next = self.current;

        if self.streak > 0 {
            self.streak -= 1;
        } else {
            self.streak = self.rand.gen::<u8>() % Self::STREAK_LIMIT;
            self.current = !self.current;
        }

        Some(next)
    }
}
