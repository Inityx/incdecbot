use std::{
    borrow::Cow,
    ops::Range,
    collections::HashSet,
};

use once_cell::sync::Lazy;

const IZZLE: &str = "izzle";
const CAPITAL_IZZLE: &str = "IZZLE";

const SHORT_INCLUSION: &[&str]  = &["be"];

static LONG_EXCLUSION: Lazy<HashSet<&'static str>> = Lazy::new(||
    [
        "mine", "our", "ours",
        "you", "your", "yours", "you're",
        "him", "she", "his", "her", "hers",
        "them", "they", "their", "theirs",
        "its",
        "the", "this", "that", "those", "these", "there",
        "for", "from", "and", "or", "but", "too", "with",

        "will", "has", "been", "can", "are", "ain't",

        "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
    ].iter().copied().collect()
);

#[allow(clippy::wrong_self_convention)] // core char methods move self
pub trait CharExt: Sized {
    fn is_vowel(self) -> bool;
    fn is_separator(self) -> bool;

    fn is_not_vowel(self) -> bool { !self.is_vowel() }
    fn is_not_separator(self) -> bool { !self.is_separator() }
}

impl CharExt for char {
    fn is_vowel(self) -> bool { "aeiouyAEIOUY".contains(self) }
    fn is_separator(self) -> bool { self.is_whitespace() || (self.is_ascii_punctuation() && self != '\'') }
}

#[derive(Debug, Clone)]
pub struct Word<'a> {
    text: &'a str,
    lowercase: String,
}

impl<'a> Word<'a> {
    pub fn izzled(src: &'a str) -> Cow<'a, str> {
        match Self::try_izzle(src) {
            Some(izzled) => Cow::Owned(izzled),
            None => Cow::Borrowed(src),
        }
    }

    pub fn try_izzle(text: &'a str) -> Option<String> {
        debug_assert!(!text.chars().any(char::is_separator), "Word text {:?} contains separators", text);

        // sdjfkl removal
        if text.chars().all(char::is_not_vowel) { return None; }

        let lowercase = text.to_ascii_lowercase();

        // initial exemption
        if text.len() < 3 && !SHORT_INCLUSION.contains(&lowercase.as_str()) { return None; }
        if LONG_EXCLUSION.contains(lowercase.as_str()) { return None; }

        let word = Self { text, lowercase };

        // apostrophe removal
        let (main_len, suffix) = word.de_apostrophize();
        if main_len < 2 { return None; }
        if LONG_EXCLUSION.contains(&word.lowercase[..main_len]) { return None; }

        // suffix removal
        let (prefix_len, suffix) = word.split_suffix(main_len, suffix);
        if word.lowercase[..prefix_len].ends_with("izzl") { return None; }

        // conversion
        Some(word.convert(prefix_len) + &suffix)
    }

    fn convert(&self, main_len: usize) -> String {
        let (prefix, all_caps) = match self.final_vowel_cluster_before(main_len) {
            None => (self.text, false),
            Some(Range{ start, end }) => {
                let prefix = match &self.lowercase[start..end] {
                    "ia" | "io" | "ie" | "ua" => &self.text[..start + 1],
                    "oui" => &self.text[..start + 2],
                    _ => &self.text[..start],
                };
                let suffix_has_lowercase = &self.text[start..].chars().any(char::is_lowercase);

                (prefix, !suffix_has_lowercase)
            }
        };

        let izzle = if all_caps { CAPITAL_IZZLE } else { IZZLE };
        prefix.to_string() + izzle
    }

    fn de_apostrophize(&self) -> (usize, String) {
        // chop after first mid-word apostrophe
        let end = self.text.match_indices('\'')
            .map(|(i, _)| i)
            .find(|&i| i != 0)
            .unwrap_or(self.text.len());

        // Special case for n't
        let nt = self.text.len() > end &&
            &self.lowercase[end - 1..end] == "n" &&
            &self.lowercase[end + 1..end + 2] == "t";

        let end = if nt { end - 1 } else { end };
        let suffix = String::from(&self.text[end..]);

        (end, suffix)
    }

    fn split_suffix(&self, end: usize, mut suffix: String) -> (usize, String) {
        // S-depluralize
        let end = match self.len_without_before(end, "s") {
            Some(len) if !self.lowercase.ends_with("ous") => {
                suffix.insert_str(0, &self.text[len..]);
                len
            }
            _ => end,
        };

        // Simple past tense
        if let Some(len) = self.len_without_before(end, "ed") {
            suffix.insert_str(0, &self.text[len + 1..len + 2]); // Drop e as 'izzle' already ends with one
            return (len, suffix);
        }

        if self.text.len() > 2 {
            if let Some(len) = self.len_without_first_before(end, &["er", "est", "e", "tion"]) {
                return (len, suffix);
            }
        }

        (end, suffix)
    }

    fn len_without_before(&self, end: usize, suffix: &str) -> Option<usize> {
        debug_assert!(!suffix.chars().any(char::is_uppercase), "Suffix {:?} contains uppercase letters", suffix);
        if !self.lowercase[..end].ends_with(suffix) { return None; }

        Some(end - suffix.len())
    }

    fn len_without_first_before(&self, end: usize, suffixes: &[&str]) -> Option<usize> {
        suffixes.iter().find_map(|suffix| self.len_without_before(end, suffix))
    }

    fn final_vowel_cluster_before(&self, end: usize) -> Option<Range<usize>> {
        let last_vowel = self.text[..end].rfind(char::is_vowel)?; // No vowels found -> return None

        let first_vowel = match self.text[..last_vowel].rfind(char::is_not_vowel) {
            Some(last_cons) => last_cons + 1,
            None => 0, // No consonants found -> Vowel cluster starts word
        };

        Some(first_vowel..last_vowel + 1)
    }
}
