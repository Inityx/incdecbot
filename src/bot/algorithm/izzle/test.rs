use super::izzle;

#[derive(Clone, Copy)]
struct Conversion<'a> {
    original: &'a str,
    expected: &'a str,
}

impl Conversion<'_> {
    fn is_correct(&self) -> bool {
        let computed = izzle(self.original);

        if computed != self.expected {
            eprintln!(
                "> {}\nexp\t{}\nwas\t{}",
                self.original,
                self.expected.escape_debug(),
                computed.escape_debug(),
            );
        }

        computed == self.expected
    }
}

macro_rules! conversions {
    ([$($word:ident)+]) => {
        [$(
            Conversion {
                original: stringify!($word),
                expected: stringify!($word),
            },
        )+]
    };
    ([$($original:ident: $expected:ident,)+]) => {
        [$(
            Conversion {
                original: stringify!($original),
                expected: stringify!($expected),
            },
        )+]
    };
    ([$($original:expr => $expected:expr,)+]) => {
        [$(
            Conversion {
                original: $original,
                expected: $expected,
            },
        )+]
    };
}

macro_rules! assert_convert {
    ($name:ident => $conversions:tt) => {
        #[test]
        fn $name() {
            let conversions = conversions!($conversions);
            assert!(!conversions.is_empty());

            let count = conversions.iter()
                .filter(|c| !c.is_correct())
                .count();

            assert_eq!(0, count);
        }
    }
}

#[test]
fn single_letters() {
    let lower_case = 'a'..='z';
    let upper_case = 'A'..='Z';
    let numbers = '0'..='9';

    let all = lower_case.chain(upper_case).chain(numbers);

    for ch in all.map(String::from) {
        let converted = izzle(&ch);
        assert_eq!(ch, converted);
    }
}

assert_convert! { special => [
    "" => "",
    "      " => "      ",
]}

assert_convert! { punctuation => [
    "Hello!" => "Hellizzle!",
    "dog's" => "dizzle's",
    "#owned" => "#izzled",
    "won't" => "wizzlen't",
]}

assert_convert! { sentence_simple => [
    "My name is Tom" => "My nizzle is Tizzle",
    "The quick brown fox" => "The qizzle brizzle fizzle",
]}

assert_convert! { sentence_intermixed => [
    "  Whitespace    preservation " => "  Whitespizzle    preservizzle ",
    "snake_case_variables" => "snizzle_cizzle_variizzles",
    "You're going to have a bad time" => "You're gizzle to hizzle a bizzle tizzle",
    "Y'all ain't \"gone\"" => "Y'all ain't \"gizzle\"",
    "I'm gonna be" => "I'm gonnizzle bizzle",
    "There aren't words that are too complex" => "There aren't wizzles that are too complizzle",
]}

assert_convert! { casing => [
    "This is SHOUTING" => "This is SHOUTIZZLE",
    "endINGS cAN sTILL bE CapiTalIZED" => "endIZZLES cAN sTIZZLE bIZZLE CapiTalIZZLED",
    "COMMUNICATION" => "COMMUNICIZZLE",
]}

assert_convert! { usernames => [
    "@UserName" => "@UserNizzle",
    "@PascalCaseShouldNotSplit" => "@PascalCaseShouldNotSplizzle",
]}

assert_convert! { unchanged_words => [
    you your yours
    me my mine
    he his
    she her hers
    them they their theirs
    us our ours
    it its
    the an one
    the this that those
]}

assert_convert! { alphanumeric_words => [
    hello: hellizzle,
    file: fizzle,
    correct: corrizzle,
    case: cizzle,
    actuarial: actuariizzle,
    cheesier: cheesizzle,
    famous: famizzle,
    family: familizzle,
    elbowing: elbowizzle,
    exabyte: exabizzle,
    caribou: caribizzle,
    Chelsea: Chelsizzle,
    Cascadia: Cascadiizzle,
    Bernie: Bernizzle,
    dystopia: dystopiizzle,
    employee: emplizzle,
    shoe: shizzle,
    shoot: shizzle,
    meme: mizzle,
    man: mizzle,
    woman: womizzle,
    hour: hizzle,
    Jeshua: Jeshuizzle,
    someone: somizzle,
    crazy: crazizzle,
    own: izzle,
    articulation: articulizzle,
    split: splizzle,
    dye: dizzle,
    add: izzle,

    hellos: hellizzles,
    files: fizzles,
    corrects: corrizzles,
    cases: cizzles,
    actuarials: actuariizzles,
    families: familizzles,
    elbowings: elbowizzles,
    exabytes: exabizzles,
    Chelseas: Chelsizzles,
    Cascadias: Cascadiizzles,
    Bernies: Bernizzles,
    dystopias: dystopiizzles,
    employees: emplizzles,
    shoes: shizzles,
    shoots: shizzles,
    memes: mizzles,
    hours: hizzles,
    Jeshuas: Jeshuizzles,
    crazies: crazizzles,
    owns: izzles,
    articulations: articulizzles,
    splits: splizzles,
    dyes: dizzles,
    adds: izzles,

    be: bizzle,
    kazaa: kazizzle,
    vertibrae: vertibrizzle,
    dubai: dubizzle,
    Mao: Mizzle,
    Obama: Obamizzle,
    chateau: chatizzle,
    spray: sprizzle,
    goatee: goatizzle,
    Taipei: Taipizzle,
    Romeo: Romizzle,
    pulley: pullizzle,
    selfie: selfizzle,
    Louie: Louizzle,
    Wii: Wizzle,
    folio: foliizzle,
    radio: radiizzle,
    barbacoa: barbacizzle,
    Monroe: Monrizzle,
    boy: bizzle,
    boi: bizzle,
    cockatoo: cockatizzle,
    supported: suppizzled,
]}
