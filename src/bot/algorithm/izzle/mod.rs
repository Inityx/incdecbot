use std::borrow::Cow;

#[cfg(test)] mod test;

mod word;
use word::{Word, CharExt};

pub fn izzle(text: &str) -> String {
    Chop(text).collect()
}

struct Chop<'a>(&'a str);

impl<'a> Iterator for Chop<'a> {
    type Item = Cow<'a, str>;

    fn next(&mut self) -> Option<Self::Item> {
        let first_char = self.0.chars().next()?;

        let (segment, src_len) = if first_char.is_separator() {
            let src = self.0.split(char::is_not_separator).next().unwrap();
            (Cow::Borrowed(src), src.len())
        } else {
            let src = self.0.split(char::is_separator).next().unwrap();
            (Word::izzled(src), src.len())
        };

        // Advance
        self.0 = &self.0[src_len..];

        Some(segment)
    }
}
