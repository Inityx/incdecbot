use super::*;

#[test]
fn ascii() {
    let r = Rotated::sentence("hi");
    assert_eq!(
        Ok(Rotated::from_blocks_unchecked(&[
            Block::slim("h"),
            Block::slim("i"),
        ])),
        r,
    );

    assert_eq!(
        concat!(
            "h i \n",
            "i h ",
        ),
        r.unwrap().to_string(),
    );
}

#[test]
fn multiple_words() {
    let r = Rotated::sentence("foo bar");

    assert_eq!(
        Ok(Rotated::from_blocks_unchecked(&[
            Block::slim("f"),
            Block::slim("o"),
            Block::slim("o"),
            Block::wide("  "),
            Block::slim("b"),
            Block::slim("a"),
            Block::slim("r"),
            Block::wide("  "),
        ])),
        r,
    );

    assert_eq!(
        concat!(
            "f o o   b a r   \n",
            "o o   b a r   f \n",
            "o   b a r   f o \n",
            "  b a r   f o o \n",
            "b a r   f o o   \n",
            "a r   f o o   b \n",
            "r   f o o   b a \n",
            "  f o o   b a r ",
        ),
        r.unwrap().to_string(),
    );
}

#[test]
fn emoji_joiner() {
    const B: &str = "\u{1F171}\u{FE0F}";
    let source_string = format!("{B}oo");
    let r = Rotated::sentence(source_string.as_str());

    assert_eq!(
        Ok(Rotated::from_blocks_unchecked(&[
            Block::wide(B),
            Block::slim("o"),
            Block::slim("o"),
        ])),
        r,
    );

    assert_eq!(
        concat!(
            "\u{1F171}\u{FE0F}o o \n",
            "o o \u{1F171}\u{FE0F}\n",
            "o \u{1F171}\u{FE0F}o ",
        ),
        r.unwrap().to_string(),
    );
}

#[ignore]
#[test]
fn emoji_no_joiner() {
    const B: &str = "\u{1F171}";
    let source_string = format!("{B}oo");
    let r = Rotated::sentence(source_string.as_str());

    assert_eq!(
        Ok(Rotated::from_blocks_unchecked(&[
            Block::wide(B),
            Block::slim("o"),
            Block::slim("o"),
        ])),
        r,
    );

    assert_eq!(
        concat!(
            "\u{1F171}o o \n",
            "o o \u{1F171}\n",
            "o \u{1F171}o ",
        ),
        r.unwrap().to_string(),
    );
}

#[test]
fn lenny_face() {
    let r = Rotated::sentence("( ͡° ͜ʖ ͡°)");

    assert_eq!(
        Ok(Rotated::from_blocks_unchecked(&[
            Block::slim("("),
            Block::wide("  "),
            Block::slim("͡"),
            Block::slim("°"),
            Block::wide("  "),
            Block::slim("͜"),
            Block::slim("ʖ"),
            Block::wide("  "),
            Block::slim("͡"),
            Block::slim("°"),
            Block::slim(")"),
            Block::wide("  "),
        ])),
        r,
    );
}
