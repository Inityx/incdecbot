use std::fmt;
use block::Block;

#[cfg(test)] mod test;
mod block;

const MAX_WIDTH: usize = 30;

#[derive(PartialEq, Eq)]
pub enum Error {
    TooLong(usize),
    Empty,
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::TooLong(length) => write!(f,
                "Trimmed input had too many elements ({}/{})",
                length, MAX_WIDTH
            ),
            Self::Empty => f.write_str("Trimmed input was empty"),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::TooLong(length) => write!(f, "Too long ({}/{MAX_WIDTH})", length),
            Self::Empty => f.write_str("Rotaten't"),
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct Rotated<'src>(Vec<Block<'src>>);

impl<'src> Rotated<'src> {
    pub fn sentence(sentence: &'src str) -> Result<Self, Error> {
        let blocks = Block::split_sentence(sentence);

        match blocks.len() {
            1..=MAX_WIDTH => Ok(Self::from_blocks_unchecked(&blocks)),
            0 => Err(Error::Empty),
            length => Err(Error::TooLong(length)),
        }
    }

    fn from_blocks_unchecked(blocks: &[Block<'src>]) -> Self {
        Self(blocks.repeat(2))
    }
}

impl fmt::Display for Rotated<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use itertools::Itertools;

        let len = self.0.len() / 2;
        if len == 0 { return Ok(()) }

        self.0
            .windows(len)
            .take(len)
            .map(|window| window.iter().format(""))
            .format("\n")
            .fmt(f)
    }
}
