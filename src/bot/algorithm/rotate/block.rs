use std::fmt;
use unicode_segmentation::UnicodeSegmentation;
use unicode_width::UnicodeWidthStr;

const VARIATION_SELECTOR: char = '\u{fe0f}';

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Block<'src> {
    grapheme: &'src str,
    padded: bool,
}

impl<'src> Block<'src> {
    const SPACE: Block<'static> = Block::wide("  ");

    #[allow(dead_code)] // used in testing
    pub const fn slim(grapheme: &'src str) -> Self {
        Self { grapheme, padded: true }
    }

    pub const fn wide(grapheme: &'src str) -> Self {
        Self { grapheme, padded: false }
    }

    pub fn split_sentence(sentence: &'src str) -> Vec<Self> {
        let sentence = sentence.trim();
        if sentence.is_empty() { return Vec::new(); }

        let words: Vec<_> = sentence.split_whitespace().collect();
        let spaces = words.len() - 1;

        let mut blocks = Vec::with_capacity(sentence.len() + spaces);

        for word in words {
            blocks.extend(Self::convert(word));
            if spaces > 0 { blocks.push(Self::SPACE); }
        }

        blocks
    }

    fn convert(src: &'src str) -> impl 'src + Iterator<Item=Block<'src>> {
        src.graphemes(true).map(Self::from_grapheme)
    }

    fn from_grapheme(grapheme: &'src str) -> Self {
        debug_assert_eq!(1, grapheme.graphemes(true).count());
        let wide = grapheme.width() > 1 || grapheme.contains(VARIATION_SELECTOR);
        Self { grapheme, padded: !wide }
    }
}

impl fmt::Debug for Block<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.padded {
            write!(f, "({}, \" \")", self.grapheme)
        } else {
            write!(f, "({})", self.grapheme)
        }
    }
}

impl fmt::Display for Block<'_> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.write_str(self.grapheme)?;
        if self.padded { fmt.write_str(" ")?; }
        Ok(())
    }
}
