use std::fmt;

pub mod rotate;
pub mod mock;
pub mod izzle;
pub mod zalgo;
pub mod dingle;

pub struct Truncate<'a> {
    text: &'a str,
    ellipsis: bool,
}

impl<'a> Truncate<'a> {
    const USER_STRING_LIMIT: usize = 120;

    pub fn to_log(s: &'a str) -> Self {
        Self::at(s, Self::USER_STRING_LIMIT)
    }

    pub fn at(s: &'a str, limit: usize) -> Self {
        let trimmed = &s[..s.floor_char_boundary(limit)];

        let text = match trimmed.find('\n') {
            Some(line_break) => &trimmed[..line_break],
            None => trimmed,
        };

        Self {
            text,
            ellipsis: text.len() < s.len(),
        }
    }
}

impl fmt::Debug for Truncate<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.ellipsis {
            write!(f, "\"{}...\"", self.text.escape_debug())
        } else {
            write!(f, "{:?}", self.text)
        }
    }
}

impl fmt::Display for Truncate<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.ellipsis {
            write!(f, "{}...", self.text)
        } else {
            f.write_str(self.text)
        }
    }
}
