use std::ops::Range;
use rand::{Rng, thread_rng, seq::SliceRandom};
use once_cell::sync::Lazy;

const AMOUNT: Range<usize> = 4..16;

static INSIDE: Lazy<Box<[char]>> = Lazy::new(|| ('\u{334}'..'\u{339}').collect());
static OUTSIDE: Lazy<Box<[char]>> = Lazy::new(||
    ('\u{300}'..'\u{334}')
        .chain('\u{339}'..'\u{362}')
        .collect()
);

pub fn zalgo(text: &str) -> String {
    let mut rng = thread_rng();
    let mut output = String::with_capacity(text.len() * AMOUNT.start);

    for ch in text.chars() {
        output.push(ch);

        if !ch.is_whitespace() {
            let count = rng.gen_range(AMOUNT);
            output.push(*INSIDE.choose(&mut rng).unwrap());
            output.extend(OUTSIDE.choose_multiple(&mut rng, count));
        }
    }

    output
}
