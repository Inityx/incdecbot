
use rand::{Rng, prelude::{SliceRandom, Distribution}, distributions::Standard};

const FIRST_CONS: &[&str] = &["Qu", "Ju", "Fl", "Br", "Bl", "W", "G", "Gr"];
const LAST_CONS: &[&str] = &["D", "P", "Pr", "Cr", "T", "Br", "F", "Fr", "Z"];
const LAST_VOWEL: &[&str] = &["a", "i", "o", "u", ];

const FIRST_PREFIX: &[&str] = &["Bing", "Spank", "Dong", "Dip", "Yarf"];
const FIRST_SUFFIX: &[&str] = &["leton", "hauer", "endorf", "berto"];
const LAST_PREFIX: &[&str] = &["Coochie", "Donk", "Dingle", "Honk"];
const LAST_SUFFIX: &[&str] = &["smith", "bilfer", "smeller", "scronkle"];

pub fn generate() -> String {
    let mut rand = rand::thread_rng();

    let first = match rand.gen() {
        First::Quandale => format!(
            "{}andale",
            FIRST_CONS.choose(&mut rand).unwrap()
        ),
        First::Bingleton => format!(
            "{}{}",
            FIRST_PREFIX.choose(&mut rand).unwrap(),
            FIRST_SUFFIX.choose(&mut rand).unwrap(),
        ),
    };

    let last = match rand.gen() {
        Last::Dingle => format!(
            "{}{}ngle",
            LAST_CONS.choose(&mut rand).unwrap(),
            LAST_VOWEL.choose(&mut rand).unwrap(),
        ),
        Last::BingleDingle => {
            let vowel = LAST_VOWEL.choose(&mut rand).unwrap();
            let cons1 = LAST_CONS.choose(&mut rand).unwrap();
            let cons2 = loop {
                let cons2 = LAST_CONS.choose(&mut rand).unwrap();
                if cons2 != cons1 { break cons2; }
            };

            format!(
                "{}{}ngle {}{}ngle",
                cons1, vowel, cons2, vowel
            )
        }
        Last::Smith => format!(
            "{}{}",
            LAST_PREFIX.choose(&mut rand).unwrap(),
            LAST_SUFFIX.choose(&mut rand).unwrap(),
        ),
    };

    first + " " + &last
}

enum First { Quandale, Bingleton }

impl Distribution<First> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> First {
        match rng.next_u32() % 10 {
            0..=5 => First::Quandale,
            6..=9 => First::Bingleton,
            _ => unreachable!(),
        }
    }
}

enum Last { Dingle, BingleDingle, Smith }

impl Distribution<Last> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Last {
        match rng.next_u32() % 10 {
            0..=4 => Last::Dingle,
            5..=7 => Last::Smith,
            8..=9 => Last::BingleDingle,
            _ => unreachable!(),
        }
    }
}

#[cfg(test)]
mod test {
    use super::generate;
    use std::iter;

    #[test]
    fn print_10() {
        for name in iter::repeat_with(generate).take(10) {
            println!("{name}");
        }
    }
}
