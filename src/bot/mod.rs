use std::{borrow::Cow, sync::{Arc, Mutex}, time::Duration};
use frankenstein::AsyncTelegramApi;
use tokio::sync::broadcast;

use crate::{tg::traits::UserExt, util::Metrics};

mod command;
mod algorithm;
pub mod db;

use db::photo::{PhotoDB, ImageMetadata};
use command::Command;

pub trait MessageAction {
    type Error;
    async fn reply_html(&self, text: String, metrics: &mut Metrics) -> Result<(), Self::Error>;
    async fn send_html(&self, text: String, metrics: &mut Metrics) -> Result<(), Self::Error>;
    async fn send_photo(&self, file: ImageMetadata, bot: &BotState, metrics: &mut Metrics) -> Result<(), Self::Error>;
}

pub struct TelegramAction<'a> {
    pub api: &'a frankenstein::AsyncApi,
    pub message: &'a frankenstein::Message,
}

impl<'a> TelegramAction<'a> {
    pub fn new(api: &'a frankenstein::AsyncApi, message: &'a frankenstein::Message) -> Self {
        Self { api, message }
    }
}

impl MessageAction for TelegramAction<'_> {
    type Error = frankenstein::Error;

    async fn reply_html(&self, text: String, metrics: &mut Metrics) -> Result<(), Self::Error> {
        println!("INFO: Replying to Telegram message");
        let _time = metrics.time_framework();
        let params = frankenstein::SendMessageParams::builder()
            .chat_id(self.message.chat.id)
            .text(text)
            .reply_to_message_id(self.message.message_id)
            .allow_sending_without_reply(false)
            .parse_mode(frankenstein::ParseMode::Html)
            .build();

        self.api.send_message(&params).await.map(drop)
    }

    async fn send_html(&self, text: String, metrics: &mut Metrics) -> Result<(), Self::Error> {
        println!("INFO: Sending Telegram message");
        let _time = metrics.time_framework();
        let params = frankenstein::SendMessageParams::builder()
            .chat_id(self.message.chat.id)
            .text(text)
            .parse_mode(frankenstein::ParseMode::Html)
            .build();

        self.api.send_message(&params).await.map(drop)
    }

    async fn send_photo(&self, file: ImageMetadata, bot: &BotState, metrics: &mut Metrics) -> Result<(), Self::Error> {
        println!("INFO: Sending Telegram photo");
        let _time = metrics.time_framework();
        let file_to_mark = file.tg_file_id.is_none().then(|| file.path.clone());

        let params = frankenstein::SendPhotoParams::builder()
            .chat_id(self.message.chat.id)
            .photo(file.clone())
            .build();

        let sent_message = self.api.send_photo(&params).await?.result;
        let attachment = sent_message.photo
            .and_then(|photos| photos.into_iter().next())
            .expect("Sent photo message has no photo??")
            .file_id;

        if let Some(path) = file_to_mark {
            bot.nut_photos.lock().unwrap().mark_cached_tg(&path, attachment);
        }

        Ok(())
    }
}


pub struct BotState {
    pub tg_name: String,
    pub nut_photos: Mutex<PhotoDB>,
}

impl BotState {
    fn db_persist(&self, force: bool) {
        self.nut_photos.lock().unwrap().sync_fs(force);
    }
}

pub struct Bot {
    pub tg_api: frankenstein::AsyncApi,
    pub state: BotState,
}

impl Bot {
    const SYNC_PERIOD: Duration = Duration::from_secs(5 * 60);
    const TG_ALLOWED_UPDATES: &'static [frankenstein::AllowedUpdate] = &[frankenstein::AllowedUpdate::Message];
    const POLL_TIMEOUT: Duration = Duration::from_secs(30);

    pub async fn new(tg_api: frankenstein::AsyncApi, nut_photos: PhotoDB) -> Self {
        let tg_name = tg_api.get_me().await.expect("Error querying bot info")
            .result.display_name();

        println!("INFO: Validated Telegram credentials for {tg_name}");
        let state = BotState {
            tg_name,
            nut_photos: Mutex::new(nut_photos),
        };

        Self {
            tg_api,
            state
        }
    }

    pub async fn run(self) {
        let bot = Arc::new(self);
        let (halt_tx, halt_rx) = broadcast::channel(1);

        println!("INFO: Starting db worker");
        let db_task = tokio::spawn(db_persister(Arc::clone(&bot), halt_tx.subscribe()));

        println!("INFO: Starting Telegram worker");
        let tg_task = tokio::spawn(tg_worker(Arc::clone(&bot), halt_rx));

        println!("INFO: Registering SIGINT handler");
        ctrlc::set_handler(move || {
            println!("INFO: Caught SIGINT");
            _ = halt_tx.send(());
        }).unwrap();

        println!("INFO: Now serving messages");

        let _ = tg_task.await;
        let _ = db_task.await;
    }

    async fn respond_to_tg(self: Arc<Self>, message: frankenstein::Message) {
        let mut metrics = Metrics::new();
        let Some(command) = command::CommandDispatch::parse(&message, &self.state.tg_name) else { return; };

        let sender = message.from.as_deref()
            .map(|user| Cow::Owned(user.display_name()))
            .unwrap_or(Cow::Borrowed("channel"));

        let result = match command {
            Ok(command) => {
                println!("INFO: Received {sender}'s {command}");
                command.execute(TelegramAction::new(&self.tg_api, &message), &self.state, &mut metrics).await
            }
            Err(command) => {
                println!("INFO: Unknown command {command:?} from {sender}");
                TelegramAction::new(&self.tg_api, &message).reply_html("WHOMST'D'VE".to_string(), &mut metrics).await
            }
        };

        if let Err(e) = result {
            println!("WARN: Error handling Telegram mesage: {e}");
        }
    }
}

async fn tg_worker(bot: Arc<Bot>, mut halt: broadcast::Receiver<()>) {
    let mut params = frankenstein::GetUpdatesParams::builder()
        .timeout(Bot::POLL_TIMEOUT.as_secs() as u32)
        .allowed_updates(Bot::TG_ALLOWED_UPDATES)
        .build();

    loop { tokio::select! {
        _ = halt.recv() => break println!("INFO: Halting Telegram task"),
        response = bot.tg_api.get_updates(&params) => {
            let updates = match response {
                Ok(response) => response.result,
                Err(e) => {
                    println!("ERROR: Telegram connection error: {e}");
                    continue;
                }
            };

            if let Some(max_id) = updates.iter().map(|update| update.update_id).max() {
                params.offset = Some(i64::from(max_id) + 1);
            }

            for update in updates {
                use frankenstein::{Update, UpdateContent};
                if let Update { content: UpdateContent::Message(message), .. } = update {
                    tokio::spawn(Arc::clone(&bot).respond_to_tg(message));
                }
            }
        }
    }}
}

async fn db_persister(bot: Arc<Bot>, mut halt: broadcast::Receiver<()>) {
    let mut interval = tokio::time::interval(Bot::SYNC_PERIOD);

    loop { tokio::select! {
        _ = halt.recv() => break println!("INFO: Halting database task"),
        _ = interval.tick() => bot.state.db_persist(false),
    }}
    bot.state.db_persist(false);
}
